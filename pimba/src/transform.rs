use crate::math::Vec2;

#[derive(Debug, Clone, PartialEq)]
pub struct Transform {
    pub position: Vec2,
}
