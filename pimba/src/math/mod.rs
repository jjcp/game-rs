use std::ops::{Add, AddAssign, Mul, MulAssign};

#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct Vec2 {
    pub x: f32,
    pub y: f32,
}

impl Vec2 {
    pub fn new(x: f32, y: f32) -> Vec2 {
        Vec2 { x: x, y: y }
    }

    pub fn length_squared(&self) -> f32 {
        self.x * self.x + self.y * self.y
    }

    pub fn length(&self) -> f32 {
        self.length_squared().sqrt()
    }

    pub fn normalized(&self) -> Vec2 {
        Vec2 {
            x: self.x / self.length(),
            y: self.y / self.length(),
        }
    }

    pub fn rounded(&self) -> Vec2 {
        Vec2 {
            x: self.x.round(),
            y: self.y.round(),
        }
    }
}

impl Mul<f32> for Vec2 {
    type Output = Vec2;

    fn mul(self, rhs: f32) -> Vec2 {
        Vec2 {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}

impl MulAssign<f32> for Vec2 {
    fn mul_assign(&mut self, rhs: f32) {
        *self = Vec2 {
            x: self.x * rhs,
            y: self.y * rhs,
        };
    }
}

impl Mul<Vec2> for f32 {
    type Output = Vec2;

    fn mul(self, rhs: Vec2) -> Vec2 {
        Vec2 {
            x: self * rhs.x,
            y: self * rhs.y,
        }
    }
}

impl Add<Vec2> for Vec2 {
    type Output = Vec2;

    fn add(self, rhs: Vec2) -> Vec2 {
        Vec2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl AddAssign for Vec2 {
    fn add_assign(&mut self, other: Vec2) {
        *self = Vec2 {
            x: self.x + other.x,
            y: self.y + other.y,
        };
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_length_functions() {
        let v = Vec2 { x: 1.0, y: 0.0 };

        assert_eq!(v.length_squared(), 1.0);
        assert_eq!(v.length(), 1.0);

        let v = Vec2 { x: 2.0, y: 3.0 };

        assert_eq!(v.length_squared(), 13.0);
        assert_eq!(v.length(), 13.0f32.sqrt());
    }

    #[test]
    fn test_mul_vec2_by_scalar_value() {
        assert_eq!(Vec2::new(1.0, 0.0) * 2.0, Vec2::new(2.0, 0.0));

        assert_eq!(3.0 * Vec2::new(2.0, 3.0), Vec2::new(6.0, 9.0));
    }

    #[test]
    fn test_mul_assign_vec2_with_scalar_value() {
        let mut v = Vec2::new(2.0, 3.0);

        v *= 5.0;

        assert_eq!(v.x, 10.0);
        assert_eq!(v.y, 15.0);
    }

    #[test]
    fn test_add_vec2_with_vec2() {
        assert_eq!(
            Vec2::new(1.0, 0.0) + Vec2::new(0.0, 1.0),
            Vec2::new(1.0, 1.0)
        );

        assert_eq!(
            Vec2::new(2.0, 3.0) + Vec2::new(0.5, 0.8),
            Vec2::new(2.5, 3.8)
        );
    }

    #[test]
    fn test_add_assign_vec2_with_vec2() {
        let mut v = Vec2::new(1.0, 2.0);

        v += Vec2::new(3.0, 4.0);

        assert_eq!(v.x, 4.0);
        assert_eq!(v.y, 6.0);
    }

    #[test]
    fn test_normalization_function() {
        let v = Vec2 { x: 1.0, y: 0.0 };

        assert_eq!(v.normalized(), v);

        let v = Vec2 { x: 2.0, y: 3.0 };

        assert_eq!(v.normalized().length(), 1.0);
        assert_eq!(v.normalized() * v.length(), v);
    }

    #[test]
    fn test_rounding_function() {
        let v = Vec2 { x: 1.0, y: 2.0 };
        assert_eq!(v.rounded(), v);

        let v = Vec2 { x: 2.2, y: 3.2 };
        assert_eq!(v.rounded(), Vec2::new(2.0, 3.0));

        let v = Vec2 { x: 2.5, y: 3.5 };
        assert_eq!(v.rounded(), Vec2::new(3.0, 4.0));

        let v = Vec2 { x: 2.9, y: 3.9 };
        assert_eq!(v.rounded(), Vec2::new(3.0, 4.0));
    }
}
