use std::vec::Vec;

use super::Mapper;

use super::super::entity::Entity;
use super::super::handle::{Handle, Index};
use crate::ecs::mapper::command::MapperCommand;

pub struct VecMapper {
    to_entity: Vec<Entity>,
    to_index: Vec<Handle>,
}

impl Mapper for VecMapper {
    fn new() -> VecMapper {
        VecMapper {
            to_entity: Vec::new(),
            to_index: Vec::new(),
        }
    }

    fn entities(&self) -> &[Entity] {
        self.to_entity.as_slice()
    }

    fn index_of(&self, e: Entity) -> Option<usize> {
        let (entity_index, entity_gen) = e.handle().internal();

        if entity_index < self.to_index.len()
            && self.to_index[entity_index].generation() == entity_gen
        {
            Some(self.to_index[entity_index].internal().0)
        } else {
            None
        }
    }

    fn add(&mut self, entity: Entity) -> MapperCommand {
        if entity.is_invalid() {
            MapperCommand::Ignore
        } else {
            let (entity_index, entity_gen) = entity.handle().internal();

            if entity_index >= self.to_index.len() {
                self.to_index.resize(entity_index + 1, Handle::invalid());
            }

            let slot = self.to_index[entity_index];

            if slot.is_invalid() {
                self.to_index[entity_index] =
                    Handle::new(Index(self.to_entity.len() as u32), entity_gen);
                self.to_entity.push(entity);

                MapperCommand::Append
            } else {
                let (slot_index, _) = self.to_index[entity_index].internal();

                self.to_index[entity_index] = Handle::new(slot.index(), entity_gen);
                self.to_entity[slot_index] = entity;

                MapperCommand::Replace(slot_index)
            }
        }
    }

    fn swap_remove(&mut self, entity: Entity) -> Option<usize> {
        if !entity.is_invalid() {
            let (entity_index, entity_gen) = entity.handle().internal();

            if self.to_entity.len() > 0 && entity_index < self.to_index.len() {
                let slot = self.to_index[entity_index];
                let (slot_index, slot_gen) = slot.internal();

                if entity_gen == slot_gen {
                    let last_entity = self.to_entity[self.to_entity.len() - 1];
                    let (index, gen) = last_entity.handle().internal();
                    self.to_index[index] = Handle::new(slot.index(), gen);

                    self.to_index[entity_index] = Handle::invalid();
                    self.to_entity.swap_remove(slot_index);

                    return Some(slot_index);
                }
            }
        }

        None
    }
}

#[cfg(test)]
mod tests {
    // FIXME: There no test cases for MapperCommand::Replace(x)

    use super::*;

    use crate::ecs::handle::Generation;

    #[test]
    fn test_construction_of_an_empty_mapper() {
        let m = VecMapper::new();

        assert_eq!(m.entities().len(), 0);
    }

    #[test]
    fn test_querying_for_entities_on_an_empty_mapper() {
        let m = VecMapper::new();

        assert_eq!(m.entities().len(), 0);

        let e0 = Entity::from_handle(Handle::new(Index(0), Generation(0)));
        let e1 = Entity::from_handle(Handle::new(Index(1), Generation(0)));
        let e2 = Entity::from_handle(Handle::new(Index(2), Generation(0)));

        assert_eq!(m.index_of(e0).is_none(), true);
        assert_eq!(m.index_of(e1).is_none(), true);
        assert_eq!(m.index_of(e2).is_none(), true);

        assert_eq!(m.index_of(Entity::invalid()).is_none(), true);
    }

    #[test]
    fn test_adding_an_invalid_entity_to_an_empty_mapper() {
        let mut m = VecMapper::new();

        assert_eq!(m.add(Entity::invalid()), MapperCommand::Ignore);
        assert_eq!(m.entities().len(), 0);
    }

    #[test]
    fn test_adding_one_entity_to_an_empty_mapper() {
        let mut m = VecMapper::new();

        let e0 = Entity::from_handle(Handle::new(Index(0), Generation(0)));

        assert_eq!(m.add(e0), MapperCommand::Append);
        assert_eq!(m.index_of(e0).unwrap(), 0);
        assert_eq!(m.entities().len(), 1);
    }
    #[test]
    fn test_adding_multiple_entities() {
        let mut m = VecMapper::new();

        let e0 = Entity::from_handle(Handle::new(Index(0), Generation(0)));
        let e0_1 = Entity::from_handle(Handle::new(Index(0), Generation(1)));
        let e2 = Entity::from_handle(Handle::new(Index(2), Generation(0)));
        let e2_2 = Entity::from_handle(Handle::new(Index(2), Generation(2)));
        let e4 = Entity::from_handle(Handle::new(Index(4), Generation(0)));

        assert_eq!(m.add(e0_1), MapperCommand::Append);

        // Current State | Entity { 0, 1 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 1);

        assert_eq!(m.entities()[0], e0_1);

        assert_eq!(m.index_of(e0).is_none(), true);
        assert_eq!(m.index_of(e0_1).unwrap(), 0);
        assert_eq!(m.index_of(e2).is_none(), true);
        assert_eq!(m.index_of(e2_2).is_none(), true);
        assert_eq!(m.index_of(e4).is_none(), true);

        assert_eq!(m.index_of(Entity::invalid()).is_none(), true);

        assert_eq!(m.add(e4), MapperCommand::Append);

        // Current State | Entity { 0, 1 }
        //               | Entity { 4, 0 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 2);

        assert_eq!(m.entities()[0], e0_1);
        assert_eq!(m.entities()[1], e4);

        assert_eq!(m.index_of(e0).is_none(), true);
        assert_eq!(m.index_of(e0_1).unwrap(), 0);
        assert_eq!(m.index_of(e2).is_none(), true);
        assert_eq!(m.index_of(e2_2).is_none(), true);
        assert_eq!(m.index_of(e4).unwrap(), 1);

        assert_eq!(m.index_of(Entity::invalid()).is_none(), true);

        // Trying to map an invalid entity should do nothing
        assert_eq!(m.add(Entity::invalid()), MapperCommand::Ignore);

        // Current State | Entity { 0, 1 }
        //               | Entity { 4, 0 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 2);

        assert_eq!(m.entities()[0], e0_1);
        assert_eq!(m.entities()[1], e4);

        assert_eq!(m.index_of(e0).is_none(), true);
        assert_eq!(m.index_of(e0_1).unwrap(), 0);
        assert_eq!(m.index_of(e2).is_none(), true);
        assert_eq!(m.index_of(e2_2).is_none(), true);
        assert_eq!(m.index_of(e4).unwrap(), 1);

        assert_eq!(m.index_of(Entity::invalid()).is_none(), true);

        assert_eq!(m.add(e2), MapperCommand::Append);

        // Current State | Entity { 0, 1 }
        //               | Entity { 4, 0 }
        //               | Entity { 2, 0 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 3);

        assert_eq!(m.entities()[0], e0_1);
        assert_eq!(m.entities()[1], e4);
        assert_eq!(m.entities()[2], e2);

        assert_eq!(m.index_of(e0).is_none(), true);
        assert_eq!(m.index_of(e0_1).unwrap(), 0);
        assert_eq!(m.index_of(e2).unwrap(), 2);
        assert_eq!(m.index_of(e2_2).is_none(), true);
        assert_eq!(m.index_of(e4).unwrap(), 1);

        assert_eq!(m.index_of(Entity::invalid()).is_none(), true);
    }

    #[test]
    fn test_adding_and_removing_one_entity_case_0() {
        let mut m = VecMapper::new();

        let e0_1 = Entity::from_handle(Handle::new(Index(0), Generation(1)));

        assert_eq!(m.add(e0_1), MapperCommand::Append);

        // Current State | Entity { 0, 1 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 1);

        assert_eq!(m.entities()[0], e0_1);

        assert_eq!(m.index_of(e0_1).unwrap(), 0);

        assert_eq!(m.swap_remove(e0_1).unwrap(), 0);

        // Current State | <empty>
        // -------------------------------------------

        assert_eq!(m.entities().len(), 0);

        assert_eq!(m.index_of(e0_1).is_none(), true);
    }

    #[test]
    fn test_adding_and_removing_one_entity_case_1() {
        let mut m = VecMapper::new();

        let e5_1 = Entity::from_handle(Handle::new(Index(5), Generation(1)));

        assert_eq!(m.add(e5_1), MapperCommand::Append);

        // Current State | Entity { 5, 1 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 1);

        assert_eq!(m.entities()[0], e5_1);

        assert_eq!(m.index_of(e5_1).unwrap(), 0);

        assert_eq!(m.swap_remove(e5_1).unwrap(), 0);

        // Current State | <empty>
        // -------------------------------------------

        assert_eq!(m.entities().len(), 0);

        assert_eq!(m.index_of(e5_1).is_none(), true);
    }

    #[test]
    fn test_adding_and_removing_multiple_entities_case_0() {
        let mut m = VecMapper::new();

        let e0 = Entity::from_handle(Handle::new(Index(0), Generation(0)));
        let e1 = Entity::from_handle(Handle::new(Index(1), Generation(0)));
        let e2 = Entity::from_handle(Handle::new(Index(2), Generation(0)));
        let e3 = Entity::from_handle(Handle::new(Index(3), Generation(0)));
        let e4 = Entity::from_handle(Handle::new(Index(4), Generation(0)));

        assert_eq!(m.add(e0), MapperCommand::Append);
        assert_eq!(m.add(e1), MapperCommand::Append);
        assert_eq!(m.add(e2), MapperCommand::Append);
        assert_eq!(m.add(e3), MapperCommand::Append);
        assert_eq!(m.add(e4), MapperCommand::Append);

        // Current State | Entity { 0, 0 }
        //               | Entity { 1, 0 }
        //               | Entity { 2, 0 }
        //               | Entity { 3, 0 }
        //               | Entity { 4, 0 }
        // -------------------------------------------

        assert_eq!(m.index_of(e0).unwrap(), 0);
        assert_eq!(m.index_of(e1).unwrap(), 1);
        assert_eq!(m.index_of(e2).unwrap(), 2);
        assert_eq!(m.index_of(e3).unwrap(), 3);
        assert_eq!(m.index_of(e4).unwrap(), 4);

        assert_eq!(m.swap_remove(e3).unwrap(), 3);

        // Current State | Entity { 0, 0 }
        //               | Entity { 1, 0 }
        //               | Entity { 2, 0 }
        //               | Entity { 4, 0 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 4);

        assert_eq!(m.index_of(e0).unwrap(), 0);
        assert_eq!(m.index_of(e1).unwrap(), 1);
        assert_eq!(m.index_of(e2).unwrap(), 2);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e4).unwrap(), 3);

        assert_eq!(m.swap_remove(e0).unwrap(), 0);

        // Current State | Entity { 4, 0 }
        //               | Entity { 1, 0 }
        //               | Entity { 2, 0 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 3);

        assert_eq!(m.index_of(e0).is_none(), true);
        assert_eq!(m.index_of(e1).unwrap(), 1);
        assert_eq!(m.index_of(e2).unwrap(), 2);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e4).unwrap(), 0);

        assert_eq!(m.swap_remove(e1).unwrap(), 1);

        // Current State | Entity { 4, 0 }
        //               | Entity { 2, 0 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 2);

        assert_eq!(m.index_of(e0).is_none(), true);
        assert_eq!(m.index_of(e1).is_none(), true);
        assert_eq!(m.index_of(e2).unwrap(), 1);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e4).unwrap(), 0);

        assert_eq!(m.swap_remove(e2).unwrap(), 1);

        // Current State | Entity { 4, 0 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 1);

        assert_eq!(m.index_of(e0).is_none(), true);
        assert_eq!(m.index_of(e1).is_none(), true);
        assert_eq!(m.index_of(e2).is_none(), true);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e4).unwrap(), 0);

        assert_eq!(m.swap_remove(e4).unwrap(), 0);

        // Current State | <empty>
        // -------------------------------------------

        assert_eq!(m.entities().len(), 0);

        assert_eq!(m.index_of(e0).is_none(), true);
        assert_eq!(m.index_of(e1).is_none(), true);
        assert_eq!(m.index_of(e2).is_none(), true);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e4).is_none(), true);
    }

    #[test]
    fn test_adding_and_removing_multiple_entities_case_1() {
        let mut m = VecMapper::new();

        let e0 = Entity::from_handle(Handle::new(Index(0), Generation(0)));
        let e1 = Entity::from_handle(Handle::new(Index(1), Generation(0)));
        let e2 = Entity::from_handle(Handle::new(Index(2), Generation(0)));
        let e3 = Entity::from_handle(Handle::new(Index(3), Generation(0)));
        let e4 = Entity::from_handle(Handle::new(Index(4), Generation(0)));

        assert_eq!(m.add(e0), MapperCommand::Append);
        assert_eq!(m.add(e1), MapperCommand::Append);
        assert_eq!(m.add(e2), MapperCommand::Append);
        assert_eq!(m.add(e3), MapperCommand::Append);
        assert_eq!(m.add(e4), MapperCommand::Append);

        // Current State | Entity { 0, 0 }
        //               | Entity { 1, 0 }
        //               | Entity { 2, 0 }
        //               | Entity { 3, 0 }
        //               | Entity { 4, 0 }
        // -------------------------------------------

        assert_eq!(m.index_of(e0).unwrap(), 0);
        assert_eq!(m.index_of(e1).unwrap(), 1);
        assert_eq!(m.index_of(e2).unwrap(), 2);
        assert_eq!(m.index_of(e3).unwrap(), 3);
        assert_eq!(m.index_of(e4).unwrap(), 4);

        assert_eq!(m.swap_remove(e3).unwrap(), 3);

        // Current State | Entity { 0, 0 }
        //               | Entity { 1, 0 }
        //               | Entity { 2, 0 }
        //               | Entity { 4, 0 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 4);

        assert_eq!(m.index_of(e0).unwrap(), 0);
        assert_eq!(m.index_of(e1).unwrap(), 1);
        assert_eq!(m.index_of(e2).unwrap(), 2);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e4).unwrap(), 3);

        assert_eq!(m.swap_remove(e4).unwrap(), 3);

        // Current State | Entity { 0, 0 }
        //               | Entity { 1, 0 }
        //               | Entity { 2, 0 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 3);

        assert_eq!(m.index_of(e0).unwrap(), 0);
        assert_eq!(m.index_of(e1).unwrap(), 1);
        assert_eq!(m.index_of(e2).unwrap(), 2);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e4).is_none(), true);

        let e5 = Entity::from_handle(Handle::new(Index(5), Generation(0)));
        assert_eq!(m.add(e5), MapperCommand::Append);

        // Current State | Entity { 0, 0 }
        //               | Entity { 1, 0 }
        //               | Entity { 2, 0 }
        //               | Entity { 5, 0 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 4);

        assert_eq!(m.index_of(e0).unwrap(), 0);
        assert_eq!(m.index_of(e1).unwrap(), 1);
        assert_eq!(m.index_of(e2).unwrap(), 2);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e4).is_none(), true);
        assert_eq!(m.index_of(e5).unwrap(), 3);

        let e6 = Entity::from_handle(Handle::new(Index(6), Generation(0)));
        assert_eq!(m.add(e6), MapperCommand::Append);

        // Current State | Entity { 0, 0 }
        //               | Entity { 1, 0 }
        //               | Entity { 2, 0 }
        //               | Entity { 5, 0 }
        //               | Entity { 6, 0 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 5);

        assert_eq!(m.index_of(e0).unwrap(), 0);
        assert_eq!(m.index_of(e1).unwrap(), 1);
        assert_eq!(m.index_of(e2).unwrap(), 2);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e4).is_none(), true);
        assert_eq!(m.index_of(e5).unwrap(), 3);
        assert_eq!(m.index_of(e6).unwrap(), 4);

        // Removing e3 should do nothing (it's already been deleted)
        assert_eq!(m.swap_remove(e3).is_none(), true);

        // Current State | Entity { 0, 0 }
        //               | Entity { 1, 0 }
        //               | Entity { 2, 0 }
        //               | Entity { 5, 0 }
        //               | Entity { 6, 0 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 5);

        assert_eq!(m.index_of(e0).unwrap(), 0);
        assert_eq!(m.index_of(e1).unwrap(), 1);
        assert_eq!(m.index_of(e2).unwrap(), 2);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e4).is_none(), true);
        assert_eq!(m.index_of(e5).unwrap(), 3);
        assert_eq!(m.index_of(e6).unwrap(), 4);

        assert_eq!(m.swap_remove(e1).unwrap(), 1);

        // Current State | Entity { 0, 0 }
        //               | Entity { 6, 0 }
        //               | Entity { 2, 0 }
        //               | Entity { 5, 0 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 4);

        assert_eq!(m.index_of(e0).unwrap(), 0);
        assert_eq!(m.index_of(e1).is_none(), true);
        assert_eq!(m.index_of(e2).unwrap(), 2);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e4).is_none(), true);
        assert_eq!(m.index_of(e5).unwrap(), 3);
        assert_eq!(m.index_of(e6).unwrap(), 1);

        assert_eq!(m.swap_remove(e0).unwrap(), 0);

        // Current State | Entity { 5, 0 }
        //               | Entity { 6, 0 }
        //               | Entity { 2, 0 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 3);

        assert_eq!(m.index_of(e0).is_none(), true);
        assert_eq!(m.index_of(e1).is_none(), true);
        assert_eq!(m.index_of(e2).unwrap(), 2);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e4).is_none(), true);
        assert_eq!(m.index_of(e5).unwrap(), 0);
        assert_eq!(m.index_of(e6).unwrap(), 1);

        let e3_1 = Entity::from_handle(Handle::new(Index(3), Generation(1)));
        assert_eq!(m.add(e3_1), MapperCommand::Append);

        // Current State | Entity { 5, 0 }
        //               | Entity { 6, 0 }
        //               | Entity { 2, 0 }
        //               | Entity { 3, 1 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 4);

        assert_eq!(m.index_of(e0).is_none(), true);
        assert_eq!(m.index_of(e1).is_none(), true);
        assert_eq!(m.index_of(e2).unwrap(), 2);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e3_1).unwrap(), 3);
        assert_eq!(m.index_of(e4).is_none(), true);
        assert_eq!(m.index_of(e5).unwrap(), 0);
        assert_eq!(m.index_of(e6).unwrap(), 1);

        assert_eq!(m.swap_remove(e5).unwrap(), 0);

        // Current State | Entity { 3, 1 }
        //               | Entity { 6, 0 }
        //               | Entity { 2, 0 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 3);

        assert_eq!(m.index_of(e0).is_none(), true);
        assert_eq!(m.index_of(e1).is_none(), true);
        assert_eq!(m.index_of(e2).unwrap(), 2);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e3_1).unwrap(), 0);
        assert_eq!(m.index_of(e4).is_none(), true);
        assert_eq!(m.index_of(e5).is_none(), true);
        assert_eq!(m.index_of(e6).unwrap(), 1);

        assert_eq!(m.swap_remove(e2).unwrap(), 2);

        // Current State | Entity { 3, 1 }
        //               | Entity { 6, 0 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 2);

        assert_eq!(m.index_of(e0).is_none(), true);
        assert_eq!(m.index_of(e1).is_none(), true);
        assert_eq!(m.index_of(e2).is_none(), true);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e3_1).unwrap(), 0);
        assert_eq!(m.index_of(e4).is_none(), true);
        assert_eq!(m.index_of(e5).is_none(), true);
        assert_eq!(m.index_of(e6).unwrap(), 1);

        assert_eq!(m.swap_remove(e6).unwrap(), 1);

        // Current State | Entity { 3, 1 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 1);

        assert_eq!(m.index_of(e0).is_none(), true);
        assert_eq!(m.index_of(e1).is_none(), true);
        assert_eq!(m.index_of(e2).is_none(), true);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e3_1).unwrap(), 0);
        assert_eq!(m.index_of(e4).is_none(), true);
        assert_eq!(m.index_of(e5).is_none(), true);
        assert_eq!(m.index_of(e6).is_none(), true);

        assert_eq!(m.swap_remove(e3_1).unwrap(), 0);

        // Current State | <empty>
        // -------------------------------------------

        assert_eq!(m.entities().len(), 0);

        assert_eq!(m.index_of(e0).is_none(), true);
        assert_eq!(m.index_of(e1).is_none(), true);
        assert_eq!(m.index_of(e2).is_none(), true);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e3_1).is_none(), true);
        assert_eq!(m.index_of(e4).is_none(), true);
        assert_eq!(m.index_of(e5).is_none(), true);
        assert_eq!(m.index_of(e6).is_none(), true);

        let e0_1 = Entity::from_handle(Handle::new(Index(0), Generation(1)));
        assert_eq!(m.add(e0_1), MapperCommand::Append);

        // Current State | Entity { 0, 1 }
        // -------------------------------------------

        assert_eq!(m.entities().len(), 1);

        assert_eq!(m.index_of(e0).is_none(), true);
        assert_eq!(m.index_of(e0_1).unwrap(), 0);
        assert_eq!(m.index_of(e1).is_none(), true);
        assert_eq!(m.index_of(e2).is_none(), true);
        assert_eq!(m.index_of(e3).is_none(), true);
        assert_eq!(m.index_of(e3_1).is_none(), true);
        assert_eq!(m.index_of(e4).is_none(), true);
        assert_eq!(m.index_of(e5).is_none(), true);
        assert_eq!(m.index_of(e6).is_none(), true);
    }
}
