mod command;
mod vec;

pub use command::MapperCommand;
pub use vec::VecMapper;

use super::entity::Entity;

pub trait Mapper {
    fn new() -> Self;

    fn entities(&self) -> &[Entity];

    fn index_of(&self, e: Entity) -> Option<usize>;

    fn add(&mut self, entity: Entity) -> MapperCommand;

    fn swap_remove(&mut self, entity: Entity) -> Option<usize>;
}
