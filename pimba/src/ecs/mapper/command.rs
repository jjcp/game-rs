#[derive(Debug, PartialEq, Eq)]
pub enum MapperCommand {
    Ignore,
    Append,
    Replace(usize),
}
