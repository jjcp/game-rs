/// Basic generational index type
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Handle {
    id: u32,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Index(pub u32);

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Generation(pub u32);

impl Handle {
    const INDEX_BITS: u32 = 20;
    const INDEX_MASK: u32 = (1 << Handle::INDEX_BITS) - 1;

    pub fn new(index: Index, gen: Generation) -> Handle {
        Handle {
            id: index.0 & Handle::INDEX_MASK | gen.0 << Handle::INDEX_BITS,
        }
    }

    /// Create an invalid handle
    pub fn invalid() -> Handle {
        Handle {
            id: u32::max_value(),
        }
    }

    pub fn index(&self) -> Index {
        Index(self.id & Handle::INDEX_MASK)
    }

    pub fn generation(&self) -> Generation {
        Generation(self.id >> Handle::INDEX_BITS)
    }

    pub fn internal(&self) -> (usize, Generation) {
        (self.index().0 as usize, self.generation())
    }

    pub fn is_invalid(&self) -> bool {
        self.id == u32::max_value()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const MAX_INDEX: u32 = (1 << Handle::INDEX_BITS) - 1;
    const MAX_GEN: u32 = (1 << (32 - Handle::INDEX_BITS)) - 1;

    #[test]
    fn invalid_handle() {
        let h = Handle::invalid();

        assert_eq!(h.index(), Index(MAX_INDEX));
        assert_eq!(h.generation(), Generation(MAX_GEN));
        assert_eq!(h.is_invalid(), true);
    }

    #[test]
    fn basic_handle_construction() {
        let h = Handle::new(Index(0), Generation(0));

        assert_eq!(h.index(), Index(0));
        assert_eq!(h.generation(), Generation(0));
        assert_eq!(h.is_invalid(), false);

        let h = Handle::new(Index(10), Generation(1));

        assert_eq!(h.index(), Index(10));
        assert_eq!(h.generation(), Generation(1));
        assert_eq!(h.is_invalid(), false);
    }

    #[test]
    fn manually_construct_an_invalid_handle() {
        let h = Handle::new(Index(MAX_INDEX), Generation(MAX_GEN));

        assert_eq!(h.index(), Index(MAX_INDEX));
        assert_eq!(h.generation(), Generation(MAX_GEN));
        assert_eq!(h.is_invalid(), true);
    }

    #[test]
    fn check_index_overflow() {
        let h = Handle::new(Index(MAX_INDEX + 1), Generation(MAX_GEN));

        assert_eq!(h.index(), Index(0));
        assert_eq!(h.generation(), Generation(MAX_GEN));

        // Make sure an index overflow doesn't modify the generation part

        let h = Handle::new(Index(MAX_INDEX + 1), Generation(8));

        assert_eq!(h.index(), Index(0));
        assert_eq!(h.generation(), Generation(8));
    }

    #[test]
    fn check_generation_overflow() {
        let h = Handle::new(Index(MAX_INDEX), Generation(MAX_GEN + 1));

        assert_eq!(h.index(), Index(MAX_INDEX));
        assert_eq!(h.generation(), Generation(0));

        // Make sure a generation overflow doesn't modify the index part

        let h = Handle::new(Index(8), Generation(MAX_GEN + 1));

        assert_eq!(h.index(), Index(8));
        assert_eq!(h.generation(), Generation(0));
    }
}
