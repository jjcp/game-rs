use std::iter::Zip;
use std::slice::{Iter, IterMut};
use std::vec::Vec;

use super::entity::Entity;
use super::mapper::{Mapper, MapperCommand};

pub struct Component<T, M: Mapper> {
    components: Vec<T>,
    mapper: M,
}

impl<T, M: Mapper> Component<T, M> {
    pub fn new() -> Component<T, M> {
        Component {
            components: Vec::new(),
            mapper: M::new(),
        }
    }

    pub fn get(&self, entity: Entity) -> Option<&T> {
        match self.mapper.index_of(entity) {
            Some(index) => Some(&self.components[index]),
            None => None,
        }
    }

    pub fn get_mut(&mut self, entity: Entity) -> Option<&mut T> {
        match self.mapper.index_of(entity) {
            Some(index) => Some(&mut self.components[index]),
            None => None,
        }
    }

    pub fn iter(&self) -> Zip<Iter<T>, Iter<Entity>> {
        self.components.iter().zip(self.mapper.entities().iter())
    }

    pub fn iter_mut(&mut self) -> Zip<IterMut<T>, Iter<Entity>> {
        self.components
            .iter_mut()
            .zip(self.mapper.entities().iter())
    }

    pub fn add(&mut self, entity: Entity, data: T) {
        match self.mapper.add(entity) {
            MapperCommand::Ignore => {}
            MapperCommand::Append => {
                self.components.push(data);
            }
            MapperCommand::Replace(index) => {
                self.components[index] = data;
            }
        }
    }

    pub fn remove(&mut self, entity: Entity) {
        match self.mapper.swap_remove(entity) {
            Some(index) => {
                self.components.swap_remove(index);
            }
            None => {}
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::ecs::handle::{Generation, Handle, Index};
    use crate::ecs::mapper::VecMapper;

    #[derive(Debug, PartialEq, Eq)]
    struct Data(i32);

    #[test]
    fn test_construction_of_a_component_manager() {
        let mut c = Component::<Data, VecMapper>::new();

        assert_eq!(c.iter().count(), 0);
        assert_eq!(c.iter_mut().count(), 0);

        assert_eq!(c.get(Entity::invalid()).is_none(), true);
        assert_eq!(c.get_mut(Entity::invalid()).is_none(), true);
    }

    #[test]
    fn test_adding_and_removing_components() {
        let mut c = Component::<Data, VecMapper>::new();

        let e0 = Entity::from_handle(Handle::new(Index(0), Generation(0)));
        let e1 = Entity::from_handle(Handle::new(Index(1), Generation(0)));
        let e2 = Entity::from_handle(Handle::new(Index(2), Generation(0)));
        let e3 = Entity::from_handle(Handle::new(Index(3), Generation(0)));
        let e4 = Entity::from_handle(Handle::new(Index(4), Generation(0)));

        c.add(e0, Data(0));

        // Current State | Entity { 0, 0 } -> Data(0)
        // -------------------------------------------

        let data: Vec<(&Data, &Entity)> = c.iter().collect();
        assert_eq!(data[0], (&Data(0), &e0));

        let data: Vec<(&mut Data, &Entity)> = c.iter_mut().collect();
        assert_eq!(data[0], (&mut Data(0), &e0));

        assert_eq!(c.get(e0).unwrap(), &Data(0));
        assert_eq!(c.get(e1).is_none(), true);
        assert_eq!(c.get(e2).is_none(), true);
        assert_eq!(c.get(e3).is_none(), true);
        assert_eq!(c.get(e4).is_none(), true);

        assert_eq!(c.get_mut(e0).unwrap(), &Data(0));
        assert_eq!(c.get_mut(e1).is_none(), true);
        assert_eq!(c.get_mut(e2).is_none(), true);
        assert_eq!(c.get_mut(e3).is_none(), true);
        assert_eq!(c.get_mut(e4).is_none(), true);

        c.add(e1, Data(10));

        // Current State | Entity { 0, 0 } -> Data(0)
        //               | Entity { 1, 0 } -> Data(10)
        // -------------------------------------------

        let data: Vec<(&Data, &Entity)> = c.iter().collect();
        assert_eq!(data[0], (&Data(0), &e0));
        assert_eq!(data[1], (&Data(10), &e1));

        let data: Vec<(&mut Data, &Entity)> = c.iter_mut().collect();
        assert_eq!(data[0], (&mut Data(0), &e0));
        assert_eq!(data[1], (&mut Data(10), &e1));

        assert_eq!(c.get(e0).unwrap(), &Data(0));
        assert_eq!(c.get(e1).unwrap(), &Data(10));
        assert_eq!(c.get(e2).is_none(), true);
        assert_eq!(c.get(e3).is_none(), true);
        assert_eq!(c.get(e4).is_none(), true);

        assert_eq!(c.get_mut(e0).unwrap(), &Data(0));
        assert_eq!(c.get_mut(e1).unwrap(), &Data(10));
        assert_eq!(c.get_mut(e2).is_none(), true);
        assert_eq!(c.get_mut(e3).is_none(), true);
        assert_eq!(c.get_mut(e4).is_none(), true);

        c.add(e4, Data(40));

        // Current State | Entity { 0, 0 } -> Data(0)
        //               | Entity { 1, 0 } -> Data(10)
        //               | Entity { 4, 0 } -> Data(40)
        // -------------------------------------------

        let data: Vec<(&Data, &Entity)> = c.iter().collect();
        assert_eq!(data[0], (&Data(0), &e0));
        assert_eq!(data[1], (&Data(10), &e1));
        assert_eq!(data[2], (&Data(40), &e4));

        let data: Vec<(&mut Data, &Entity)> = c.iter_mut().collect();
        assert_eq!(data[0], (&mut Data(0), &e0));
        assert_eq!(data[1], (&mut Data(10), &e1));
        assert_eq!(data[2], (&mut Data(40), &e4));

        assert_eq!(c.get(e0).unwrap(), &Data(0));
        assert_eq!(c.get(e1).unwrap(), &Data(10));
        assert_eq!(c.get(e2).is_none(), true);
        assert_eq!(c.get(e3).is_none(), true);
        assert_eq!(c.get(e4).unwrap(), &Data(40));

        assert_eq!(c.get_mut(e0).unwrap(), &Data(0));
        assert_eq!(c.get_mut(e1).unwrap(), &Data(10));
        assert_eq!(c.get_mut(e2).is_none(), true);
        assert_eq!(c.get_mut(e3).is_none(), true);
        assert_eq!(c.get_mut(e4).unwrap(), &Data(40));

        c.remove(e1);

        // Current State | Entity { 0, 0 } -> Data(0)
        //               | Entity { 4, 0 } -> Data(40)
        // -------------------------------------------

        let data: Vec<(&Data, &Entity)> = c.iter().collect();
        assert_eq!(data[0], (&Data(0), &e0));
        assert_eq!(data[1], (&Data(40), &e4));

        let data: Vec<(&mut Data, &Entity)> = c.iter_mut().collect();
        assert_eq!(data[0], (&mut Data(0), &e0));
        assert_eq!(data[1], (&mut Data(40), &e4));

        assert_eq!(c.get(e0).unwrap(), &Data(0));
        assert_eq!(c.get(e1).is_none(), true);
        assert_eq!(c.get(e2).is_none(), true);
        assert_eq!(c.get(e3).is_none(), true);
        assert_eq!(c.get(e4).unwrap(), &Data(40));

        assert_eq!(c.get_mut(e0).unwrap(), &Data(0));
        assert_eq!(c.get_mut(e1).is_none(), true);
        assert_eq!(c.get_mut(e2).is_none(), true);
        assert_eq!(c.get_mut(e3).is_none(), true);
        assert_eq!(c.get_mut(e4).unwrap(), &Data(40));
    }
}
