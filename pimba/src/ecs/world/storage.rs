use std::vec::Vec;

use super::Entity;
use crate::ecs::handle::{Generation, Handle, Index};

const MIN_FREE_ENTITIES: u32 = 2048;

/// System for creating and destroying entities.
pub struct EntityStorage {
    handles: Vec<Handle>,

    head_index: usize,
    tail_index: usize,
    deleted_count: u32,
}

impl EntityStorage {
    pub fn new() -> EntityStorage {
        EntityStorage {
            handles: Vec::new(),
            head_index: 0,
            tail_index: 0,
            deleted_count: 0,
        }
    }

    pub fn is_valid(&self, entity: Entity) -> bool {
        let (index, _) = entity.handle().internal();

        index < self.handles.len() && self.handles[index] == entity.handle()
    }

    pub fn create_entity(&mut self) -> Entity {
        if self.deleted_count >= MIN_FREE_ENTITIES {
            // Reuse one of the previously deleted entities.

            let slot = self.handles[self.head_index];

            let new_handle = Handle::new(Index(self.head_index as u32), slot.generation());

            self.handles[self.head_index] = new_handle;

            self.head_index = slot.index().0 as usize;
            self.deleted_count -= 1;

            Entity::from_handle(new_handle)
        } else {
            // Create a new entity

            let new_handle = Handle::new(Index(self.handles.len() as u32), Generation(0));

            self.handles.push(new_handle);

            Entity::from_handle(new_handle)
        }
    }

    pub fn remove_entity(&mut self, entity: Entity) {
        if self.is_valid(entity) {
            let index = entity.handle().index().0 as usize;

            self.handles[index] = Handle::new(
                Index(index as u32 + 1),
                Generation(entity.handle().generation().0 + 1),
            );

            if self.deleted_count > 0 {
                self.handles[self.tail_index] = Handle::new(
                    Index(index as u32),
                    self.handles[self.tail_index].generation(),
                );
            } else {
                self.head_index = index;
            }

            self.tail_index = index;
            self.deleted_count += 1;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new() {
        let s = EntityStorage::new();

        assert_eq!(s.handles.len(), 0);
        assert_eq!(s.head_index, 0);
        assert_eq!(s.tail_index, 0);
        assert_eq!(s.deleted_count, 0);
    }

    #[test]
    fn test_creation_and_deletion_of_one_entity() {
        let mut s = EntityStorage::new();

        let e = s.create_entity();

        assert_eq!(s.handles.len(), 1);
        assert_eq!(s.deleted_count, 0);

        assert!(s.is_valid(e));

        s.remove_entity(e);

        assert_eq!(s.handles.len(), 1);
        assert_eq!(s.head_index, 0);
        assert_eq!(s.tail_index, 0);
        assert_eq!(s.deleted_count, 1);

        assert!(!s.is_valid(e));
    }

    // #[test]
    // fn test_creation_and_deletion_of_multiple_entities() {
    //     let mut s = EntityStorage::new();
    //     let mut stored_entities = Vec::<Entity>::new();

    //     let entity_count = 2 * MIN_FREE_ENTITIES as usize;

    //     for i in 0..entity_count {
    //         let e = s.create_entity();

    //         stored_entities.push(e);

    //         assert_eq!(s.handles.len(), i + 1);
    //         assert_eq!(s.deleted_count, 0);

    //         assert_eq!(s.is_valid(e), true);
    //     }

    //     // The index to the first deleted entity.
    //     let first_index = entity_count - 1;

    //     // Delete the last stored entity.
    //     s.remove_entity(stored_entities[first_index]);

    //     assert_eq!(s.entities.len(), entity_count);
    //     assert_eq!(s.deleted_count, 1);
    //     assert_eq!(s.head_index, first_index);
    //     assert_eq!(s.tail_index, first_index);
    //     assert_eq!(s.is_valid(stored_entities[first_index]), false);

    //     for i in 0..entity_count / 4 {
    //         let e = stored_entities[i];

    //         s.remove_entity(e);

    //         assert_eq!(s.is_valid(e), false);

    //         // Account for the first delete outside the loop
    //         assert_eq!(s.deleted_count, 1 + (i + 1) as u32);

    //         assert_eq!(s.entities.len(), entity_count);
    //         assert_eq!(s.head_index, first_index);
    //         assert_eq!(s.tail_index, e.index());
    //     }

    //     assert!(s.deleted_count < MIN_FREE_ENTITIES);

    //     // Creating a new entity while we still haven't reached the
    //     // MIN_FREE_ENTITIES must create a new (never used) entity.
    //     let e0 = s.create_entity();

    //     assert_eq!(s.entities.len(), entity_count + 1);
    //     assert_eq!(e0.index(), entity_count);
    //     assert_eq!(e0.generation(), 0);
    //     assert_eq!(s.is_valid(e0), true);

    //     // Delete more entities until s.deleted_count == MIN_FREE_ENTITIES
    //     for i in entity_count / 4..entity_count / 2 - 1 {
    //         let e = stored_entities[i];

    //         s.remove_entity(e);

    //         assert_eq!(s.is_valid(e), false);

    //         // Account for entity `e0` we created previously.
    //         assert_eq!(s.entities.len(), entity_count + 1);
    //         assert_eq!(s.head_index, first_index);
    //         assert_eq!(s.tail_index, e.index());
    //     }

    //     assert_eq!(s.deleted_count, MIN_FREE_ENTITIES);

    //     // Creating a new entity must now reuse the oldest deleted entity
    //     // i.e: the one at stored_entities[first_index]

    //     let e1 = s.create_entity();

    //     assert_eq!(e1.index(), first_index);
    //     assert_eq!(e1.generation(), 1);
    //     assert_eq!(s.is_valid(e1), true);
    //     assert_eq!(s.is_valid(stored_entities[first_index]), false);

    //     assert_eq!(s.entities.len(), entity_count + 1);
    //     assert_eq!(s.deleted_count, MIN_FREE_ENTITIES - 1);
    //     assert_eq!(s.head_index, 0);
    //     assert_eq!(s.tail_index, entity_count / 2 - 2);

    //     for i in 0..MIN_FREE_ENTITIES as usize {
    //         let e = stored_entities[i + entity_count / 2 - 1];

    //         s.remove_entity(e);

    //         assert_eq!(s.deleted_count, MIN_FREE_ENTITIES);
    //         assert_eq!(s.is_valid(e), false);
    //         assert_eq!(s.head_index, i);
    //         assert_eq!(s.tail_index, e.index());

    //         let x = s.create_entity();

    //         assert_eq!(x.index(), i);
    //         assert_eq!(x.generation(), 1);
    //         assert_eq!(s.is_valid(x), true);
    //         assert_eq!(s.is_valid(stored_entities[i]), false);

    //         assert_eq!(s.entities.len(), entity_count + 1);
    //         assert_eq!(s.deleted_count, MIN_FREE_ENTITIES - 1);
    //         assert_eq!(s.head_index, i + 1);
    //         assert_eq!(s.tail_index, e.index());
    //     }

    //     assert_eq!(s.deleted_count, MIN_FREE_ENTITIES - 1);

    //     // Creating entities when s.deleted_count != MIN_FREE_ENTITIES
    //     // should always return completely new entities.

    //     let current_length = s.entities.len();
    //     for i in 0..100 {
    //         let e = s.create_entity();

    //         assert_eq!(e.index(), current_length + i);
    //         assert_eq!(e.generation(), 0);
    //         assert_eq!(s.is_valid(e), true);

    //         assert_eq!(s.entities.len(), current_length + (i + 1));
    //         assert_eq!(s.entities[e.index()], e);
    //         assert_eq!(s.head_index, entity_count / 2);
    //         assert_eq!(s.tail_index, entity_count - 2);
    //     }
    // }
}
