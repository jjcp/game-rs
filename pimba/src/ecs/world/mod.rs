mod storage;

use super::entity::Entity;
use storage::EntityStorage;

pub struct World {
    entities: EntityStorage,
}

impl World {
    pub fn new() -> World {
        World {
            entities: EntityStorage::new(),
        }
    }

    pub fn create_entity(&mut self) -> Entity {
        self.entities.create_entity()
    }

    pub fn destroy_entity(&mut self, e: Entity) {
        self.entities.remove_entity(e)
    }

    pub fn is_alive(&self, e: Entity) -> bool {
        self.entities.is_valid(e)
    }
}
