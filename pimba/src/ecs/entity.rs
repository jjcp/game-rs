use std::fmt;

use super::handle::Handle;

/// Identifier for an entity
#[derive(Copy, Clone, PartialEq, Eq)]
pub struct Entity {
    handle: Handle,
}

impl Entity {
    pub(crate) fn from_handle(handle: Handle) -> Entity {
        Entity { handle: handle }
    }

    /// Construct an invalid entity
    pub fn invalid() -> Entity {
        Entity {
            handle: Handle::invalid(),
        }
    }

    pub fn is_invalid(&self) -> bool {
        self.handle.is_invalid()
    }

    pub fn handle(&self) -> Handle {
        self.handle
    }
}

impl fmt::Debug for Entity {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Entity {{ {index:?}, {gen:?} }}",
            index = self.handle.index(),
            gen = self.handle.generation()
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::ecs::handle::{Generation, Index};

    #[test]
    fn check_debug_fmt() {
        let e = Entity {
            handle: Handle::new(Index(1), Generation(2)),
        };

        assert_eq!(format!("{:?}", e), "Entity { Index(1), Generation(2) }");
    }

    #[test]
    fn construct_an_invalid_entity() {
        let e = Entity::invalid();

        assert_eq!(e.is_invalid(), true);
        assert_eq!(e.handle(), Handle::invalid());
    }

    #[test]
    fn get_handle_from_entity() {
        let e = Entity {
            handle: Handle::new(Index(10), Generation(20)),
        };

        assert_eq!(e.handle(), Handle::new(Index(10), Generation(20)));
    }
}
