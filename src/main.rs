pub mod anim;
pub mod app;
pub mod camera;
pub mod components;
pub mod input;
pub mod level;
pub mod physics;
pub mod player;
pub mod powerups;
pub mod render;
pub mod templates;

use sdl2::event::Event;
use sdl2::keyboard::Keycode;

use pimba::math::Vec2;

use anim::system::animation_system;
use camera::system::camera_system;
use camera::Camera;
use level::Level;
use physics::system::physics_system;
use player::system::player_system;
use render::system::{render_map, render_system};

pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window("title", 900, 600)
        .position_centered()
        .resizable()
        .maximized()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().present_vsync().build().unwrap();

    let texture_creator = canvas.texture_creator();

    let mut level = Level::from_file(&texture_creator, "assets/n0.yaml");

    let mut event_pump = sdl_context.event_pump().unwrap();

    let mut cam = Camera {
        position: Vec2::new(0.0, 0.0),
        scale: Vec2::new(1.0, 1.0),
    };

    'running: loop {
        // FIXME: This should not be here.
        let screen_width = canvas.output_size().unwrap().0 as i32;
        let screen_height = canvas.output_size().unwrap().1 as i32;
        let scale = (screen_width as f32 / 480.0).trunc();

        cam.scale = Vec2::new(scale, scale);

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => break 'running,
                Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,
                Event::KeyDown { keycode, .. } => {
                    level.game_state.input_mapper.key_pressed(keycode.unwrap());
                }
                Event::KeyUp { keycode, .. } => {
                    level.game_state.input_mapper.key_released(keycode.unwrap());
                }
                _ => {}
            }
        }

        canvas.set_draw_color(sdl2::pixels::Color::RGB(0, 255, 255));
        canvas.clear();

        player_system(
            &level.game_state.input_mapper,
            &mut level.game_state.sprite_components,
            &mut level.game_state.animation_components,
            &mut level.game_state.player_components,
            &mut level.game_state.physics_components,
            &mut level.game_state.powerups_components,
        );

        camera_system(
            1.0 / 60.0,
            // pimba::ecs::entity::Entity::invalid(),
            level.player_entity,
            &mut cam,
            &level.game_state.transform_components,
        );

        animation_system(
            1.0 / 60.0,
            &mut level.game_state.sprite_components,
            &mut level.game_state.animation_components,
        );

        physics_system(
            1.0 / 60.0,
            &level.collider_map,
            &mut level.game_state.contact_events,
            &mut level.game_state.transform_components,
            &mut level.game_state.physics_components,
            &level.game_state.collider_components,
        );

        render_map(&mut canvas, screen_width, screen_height, &cam, &level.map);

        render_system(
            &mut canvas,
            screen_width,
            screen_height,
            &cam,
            &level.texture_storage,
            &level.game_state.transform_components,
            &level.game_state.sprite_components,
        );

        canvas.present();
    }
}
