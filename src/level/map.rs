use std::vec::Vec;

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
#[repr(u8)]
pub enum Tile {
    Empty,
    Wall,
}

pub struct Map {
    map: Vec<Tile>,
    width: i32,
    height: i32,
    tile_width: i32,
    tile_height: i32,
}

impl Map {
    pub fn new(
        map_data: Vec<Tile>,
        width: i32,
        height: i32,
        tile_width: i32,
        tile_height: i32,
    ) -> Map {
        assert!((width * height) as usize == map_data.len());
        assert!(tile_width > 0);
        assert!(tile_height > 0);

        Map {
            map: map_data,
            width: width,
            height: height,
            tile_width: tile_width,
            tile_height: tile_height,
        }
    }

    pub fn width(&self) -> i32 {
        self.width
    }

    pub fn height(&self) -> i32 {
        self.height
    }

    pub fn tile_width(&self) -> i32 {
        self.tile_width
    }

    pub fn tile_height(&self) -> i32 {
        self.tile_height
    }

    pub fn at(&self, x: i32, y: i32) -> Tile {
        let index = y * self.width + x;

        if index >= 0 && index < self.map.len() as i32 {
            self.map[index as usize]
        } else {
            Tile::Empty
        }
    }
}
