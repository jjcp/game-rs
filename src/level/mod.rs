pub mod loader;
pub mod map;

use std::fs::File;
use std::io::Read;

use sdl2::render::TextureCreator;
use sdl2::surface::Surface;
use sdl2::video::WindowContext;

use pimba::ecs::entity::Entity;
use pimba::math::Vec2;
use pimba::transform::Transform;

use crate::app::GameState;
use crate::physics::collider_map::{ColliderMap, ColliderTile};
use crate::render::texture::TextureStorage;
use crate::templates::player::create_player_entity;

use map::{Map, Tile};

pub struct Level<'a> {
    pub texture_storage: TextureStorage<'a>,
    pub game_state: GameState,
    pub map: Map,
    pub collider_map: ColliderMap,
    pub player_entity: Entity,
}

impl<'a> Level<'a> {
    pub fn from_file(texture_creator: &'a TextureCreator<WindowContext>, path: &str) -> Level<'a> {
        let mut file = File::open(path).unwrap();
        let mut contents = String::new();

        file.read_to_string(&mut contents).unwrap();

        Level::from_str(texture_creator, contents.as_str())
    }

    pub fn from_str(
        texture_creator: &'a TextureCreator<WindowContext>,
        content: &str,
    ) -> Level<'a> {
        let level_info = loader::ParsedLevel::load(content);

        let mut texture_storage = TextureStorage::new();
        let mut game_state = GameState::new();
        let mut player_entity = Entity::invalid();

        let map_image = Surface::load_bmp(level_info.map.path.as_str()).unwrap();
        let (map, collider_map) = map_image.with_lock(|pixels| {
            let size = (map_image.width() * map_image.height()) as usize;

            let mut tiles = Vec::<Tile>::with_capacity(size);
            let mut colliders = Vec::<ColliderTile>::with_capacity(size);

            for (index, pixel) in pixels.iter().enumerate() {
                let x = level_info.map.tile_width * (index as i32 % map_image.width() as i32);
                let y = level_info.map.tile_height * (index as i32 / map_image.width() as i32);

                match pixel {
                    1 => {
                        tiles.push(Tile::Wall);
                        colliders.push(ColliderTile::Rect);
                    }
                    2 /* Entity: Player */ => {
                        tiles.push(Tile::Empty);
                        colliders.push(ColliderTile::Empty);

                        player_entity = create_player_entity(
                            &mut game_state,
                            Transform {
                                position: Vec2::new(x as f32, y as f32),
                            },
                            texture_storage.load_texture(texture_creator, String::from("assets/dino.bmp")),
                        );
                    }
                    _ => {
                        tiles.push(Tile::Empty);
                        colliders.push(ColliderTile::Empty);
                    }
                }

            }

            (
                Map::new(
                    tiles,
                    map_image.width() as i32,
                    map_image.height() as i32,
                    level_info.map.tile_width,
                    level_info.map.tile_height
                ),
                ColliderMap::new(
                    colliders,
                    map_image.width() as i32,
                    map_image.height() as i32,
                    level_info.map.tile_width,
                    level_info.map.tile_height
                )
            )
        });

        Level {
            texture_storage: texture_storage,
            game_state: game_state,
            map: map,
            collider_map: collider_map,
            player_entity: player_entity,
        }
    }
}
