use std::string::String;
use std::vec::Vec;

use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct ParsedLevel {
    pub map: ParsedMap,
    pub entities: Option<Vec<ParsedEntity>>,
}

impl ParsedLevel {
    pub fn load(json: &str) -> ParsedLevel {
        let d: ParsedLevel = serde_yaml::from_str(json).unwrap();
        d
    }
}

#[derive(Deserialize, Debug)]
pub struct ParsedMap {
    pub path: String,
    pub tile_width: i32,
    pub tile_height: i32,
}

#[derive(Deserialize, Debug)]
pub struct ParsedEntity {
    pub identifier: String,
    pub sprite: Option<ParsedSprite>,
}

#[derive(Deserialize, Debug)]
pub struct ParsedSprite {
    pub texture: String,
    pub x: i32,
    pub y: i32,
    pub w: i32,
    pub h: i32,
    pub flipped_x: bool,
    pub flipped_y: bool,
}
