use pimba::ecs::component::Component;
use pimba::ecs::mapper::VecMapper;
use pimba::transform::Transform;

use crate::anim::Animation;
use crate::physics::collider::Collider;
use crate::physics::Physics;
use crate::player::Player;
use crate::powerups::Powerups;
use crate::render::sprite::Sprite;

pub type TransformComponents = Component<Transform, VecMapper>;
pub type SpriteComponents = Component<Sprite, VecMapper>;
pub type AnimationComponents = Component<Animation, VecMapper>;
pub type PhysicsComponents = Component<Physics, VecMapper>;
pub type ColliderComponents = Component<Collider, VecMapper>;
pub type PlayerComponents = Component<Player, VecMapper>;
pub type PowerupsComponents = Component<Powerups, VecMapper>;
