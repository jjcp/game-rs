use pimba::ecs::entity::Entity;
use pimba::math::Vec2;
use pimba::rect::Rect;
use pimba::transform::Transform;

use crate::anim::{Animation, Clip};
use crate::app::GameState;
use crate::physics::collider::Collider;
use crate::physics::tag::Tag;
use crate::physics::Physics;
use crate::player::Player;
use crate::powerups::{Powerup, Powerups};
use crate::render::sprite::Sprite;
use crate::render::texture::TextureId;

pub fn create_player_entity(
    game_state: &mut GameState,
    transform: Transform,
    player_texture: TextureId,
) -> Entity {
    let entity = game_state.world.create_entity();

    game_state.transform_components.add(entity, transform);

    game_state.physics_components.add(
        entity,
        Physics {
            velocity: Vec2::new(0.0, 0.0),
            acceleration: Vec2::new(0.0, 900.0),
        },
    );

    game_state.sprite_components.add(
        entity,
        Sprite {
            texture: player_texture,
            rect: Rect::new(0, 0, 24, 24),
            flip_x: false,
            flip_y: false,
        },
    );

    game_state.collider_components.add(
        entity,
        Collider {
            shape: Rect::new(6, 4, 10, 17),
            tag: Tag::Unknown,
            sensor: false,
        },
    );

    game_state.animation_components.add(
        entity,
        Animation {
            play: true,
            clips: vec![
                Clip {
                    frames: vec![
                        Rect::new(24 * 0, 0, 24, 24),
                        Rect::new(24 * 1, 0, 24, 24),
                        Rect::new(24 * 2, 0, 24, 24),
                        Rect::new(24 * 3, 0, 24, 24),
                    ],
                    index: 0,
                },
                Clip {
                    frames: vec![
                        Rect::new(24 * 4, 0, 24, 24),
                        Rect::new(24 * 5, 0, 24, 24),
                        Rect::new(24 * 6, 0, 24, 24),
                        Rect::new(24 * 7, 0, 24, 24),
                        Rect::new(24 * 8, 0, 24, 24),
                        Rect::new(24 * 9, 0, 24, 24),
                    ],
                    index: 0,
                },
            ],
            seconds_per_frame: 6.0 / 60.0,
            current_counter: 0.0,
            current_clip: 1,
        },
    );

    // FIXME: This should be removed it's just to add
    //        easily, the powerup dash to the player.
    let mut powers = Powerups::new_with_capacity(8);

    powers.push(Powerup::Dash);
    powers.push(Powerup::Dash);
    powers.push(Powerup::Dash);
    powers.push(Powerup::Dash);
    powers.push(Powerup::Dash);
    powers.push(Powerup::Dash);
    powers.push(Powerup::Dash);
    powers.push(Powerup::Dash);

    game_state.powerups_components.add(entity, powers);

    // FIXME: This shouldn't be necessary.
    game_state.input_mapper.add_player_gamepad();

    game_state.player_components.add(
        entity,
        Player {
            id: 0,
            active: true,
            speed: 120.0,
            jump_speed: 200.0,
            jumping_start: None,
            jumping: false,
            dash_speed: 360.0,
            dash_duration: 150,
            dashing_start: None,
            dashing: false,
        },
    );

    entity
}
