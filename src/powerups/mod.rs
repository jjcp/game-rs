use std::collections::VecDeque;

pub struct Powerups {
    powers: VecDeque<Powerup>,
    pub max_powers: u8,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Powerup {
    Dash,
    DoubleJump,
}

impl Powerups {
    pub fn new() -> Powerups {
        Powerups {
            powers: VecDeque::new(),
            max_powers: 3,
        }
    }

    pub fn new_with_capacity(max_powers: u8) -> Powerups {
        Powerups {
            powers: VecDeque::new(),
            max_powers: max_powers,
        }
    }

    pub fn push(&mut self, power_type: Powerup) {
        if self.powers.len() < self.max_powers as usize {
            self.powers.push_back(power_type);
        }
    }

    pub fn pop(&mut self) -> Option<Powerup> {
        return self.powers.pop_front();
    }

    pub fn peek(&self) -> Option<&Powerup> {
        return self.powers.front();
    }

    pub fn next_in(&self, powers_list: &[Powerup]) -> bool {
        //! Returns true if the next power up (the given by pop) is in the
        //! list powers_list; otherwise returns false.

        if self.powers.len() == 0 {
            return false;
        }

        for power in powers_list {
            if Some(power) == self.powers.front() {
                return true;
            }
        }
        return false;
    }

    pub fn len(&self) -> u8 {
        return self.powers.len() as u8;
    }
}
