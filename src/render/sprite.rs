use pimba::rect::Rect;

use super::texture::TextureId;

#[derive(Debug)]
pub struct Sprite {
    pub texture: TextureId,
    pub rect: Rect,
    pub flip_x: bool,
    pub flip_y: bool,
}
