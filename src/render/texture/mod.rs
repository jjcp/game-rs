mod storage;

pub use storage::TextureStorage;

#[derive(Debug, Copy, Clone)]
pub struct TextureId {
    index: i32,
}

impl TextureId {
    pub fn new() -> TextureId {
        TextureId { index: -1 }
    }

    fn with_index(index: usize) -> TextureId {
        assert!(index < i32::max_value() as usize);

        TextureId {
            index: index as i32,
        }
    }

    pub fn is_valid(&self) -> bool {
        self.index >= 0
    }

    pub fn id(&self) -> i32 {
        self.index
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_construction_of_an_invalid_texture_id() {
        let x = TextureId::new();

        assert_eq!(x.id(), -1);
        assert_eq!(x.is_valid(), false);
    }

    #[test]
    fn test_construction_of_a_texture_id_with_an_index() {
        let x = TextureId::with_index(33);

        assert_eq!(x.id(), 33);
        assert_eq!(x.is_valid(), true);
    }
}
