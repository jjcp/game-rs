use std::collections::HashMap;
use std::string::String;
use std::vec::Vec;

use sdl2::render::Texture;
use sdl2::render::TextureCreator;
use sdl2::surface::Surface;
use sdl2::video::WindowContext;

use super::TextureId;

pub struct TextureStorage<'a> {
    textures: Vec<Texture<'a>>,
    map: HashMap<String, TextureId>,
}

impl<'a> TextureStorage<'a> {
    pub fn new() -> TextureStorage<'a> {
        TextureStorage {
            textures: Vec::new(),
            map: HashMap::new(),
        }
    }

    pub fn load_texture(
        &mut self,
        creator: &'a TextureCreator<WindowContext>,
        path: String,
    ) -> TextureId {
        match self.map.get(&path) {
            Some(texture_id) => *texture_id,
            None => {
                let surface = Surface::load_bmp(path.as_str()).unwrap();
                self.textures
                    .push(creator.create_texture_from_surface(surface).unwrap());

                let texture_id = TextureId::with_index(self.textures.len() - 1);
                self.map.insert(path, texture_id);

                texture_id
            }
        }
    }

    pub fn internal_texture(&self, texture_id: TextureId) -> Option<&Texture> {
        if (texture_id.id() as usize) < self.textures.len() {
            Some(&self.textures[texture_id.id() as usize])
        } else {
            None
        }
    }
}
