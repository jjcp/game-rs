use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::video::Window;

use super::texture::TextureStorage;
use crate::camera::Camera;
use crate::components::{SpriteComponents, TransformComponents};
use crate::level::map::{Map, Tile};

pub fn render_system(
    canvas: &mut Canvas<Window>,
    screen_width: i32,
    screen_height: i32,
    camera: &Camera,
    texture_storage: &TextureStorage,
    transform_components: &TransformComponents,
    sprite_components: &SpriteComponents,
) {
    let half_width = screen_width / 2;
    let half_height = screen_height / 2;

    for (sprite, e) in sprite_components.iter() {
        let transform = match transform_components.get(*e) {
            Some(x) => x,
            None => continue,
        };

        let texture = match texture_storage.internal_texture(sprite.texture) {
            Some(x) => x,
            None => continue,
        };

        match canvas.copy_ex(
            &texture,
            Rect::new(
                sprite.rect.x(),
                sprite.rect.y(),
                sprite.rect.width() as u32,
                sprite.rect.height() as u32,
            ),
            Rect::new(
                ((transform.position.x - camera.position.x) * camera.scale.x) as i32 + half_width,
                ((transform.position.y - camera.position.y) * camera.scale.y) as i32 + half_height,
                (sprite.rect.width() as f32 * camera.scale.x) as u32,
                (sprite.rect.height() as f32 * camera.scale.y) as u32,
            ),
            0.0,  /* angle */
            None, /* center */
            sprite.flip_x,
            sprite.flip_y,
        ) {
            _ => {}
        }
    }
}

pub fn render_map(
    canvas: &mut Canvas<Window>,
    screen_width: i32,
    screen_height: i32,
    camera: &Camera,
    map: &Map,
) {
    let half_width = screen_width / 2;
    let half_height = screen_height / 2;

    canvas.set_draw_color(Color::RGB(254, 91, 89));

    for i in 0..map.height() {
        for j in 0..map.width() {
            if map.at(j, i) == Tile::Wall {
                let x = (j * map.tile_width()) as f32 - camera.position.x;
                let y = (i * map.tile_height()) as f32 - camera.position.y;

                match canvas.fill_rect(Rect::new(
                    (x * camera.scale.x) as i32 + half_width,
                    (y * camera.scale.y) as i32 + half_height,
                    (map.tile_width() as f32 * camera.scale.x) as u32,
                    (map.tile_height() as f32 * camera.scale.y) as u32,
                )) {
                    _ => {}
                }
            }
        }
    }
}
