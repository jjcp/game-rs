use std::time::Instant;

pub mod system;

// FIXME: This structure is growing fast, we should create a constructor.
pub struct Player {
    pub id: u8,
    pub active: bool,
    pub speed: f32,
    pub jump_speed: f32,
    pub jumping_start: Option<Instant>,
    pub jumping: bool,
    pub dash_speed: f32,
    pub dash_duration: u32,
    pub dashing_start: Option<Instant>,
    pub dashing: bool,
}
