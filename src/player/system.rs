use std::time::Instant;

use pimba::math::Vec2;

use crate::components::{
    AnimationComponents, PhysicsComponents, PlayerComponents, PowerupsComponents, SpriteComponents,
};
use crate::input::InputMapper;
use crate::physics::Physics;
use crate::player::Player;
use crate::powerups::Powerup;

pub fn player_system(
    input_mapper: &InputMapper,
    sprite_components: &mut SpriteComponents,
    animation_components: &mut AnimationComponents,
    player_components: &mut PlayerComponents,
    physics_components: &mut PhysicsComponents,
    powerups_components: &mut PowerupsComponents,
) {
    for (player, e) in player_components.iter_mut() {
        if player.active {
            let physics = match physics_components.get_mut(*e) {
                Some(x) => x,
                None => continue,
            };

            let player_gamepad = input_mapper.get_player_gamepad(player.id);

            let jumping_start = match player.jumping_start {
                Some(x) => x,
                None => Instant::now(),
            };

            let dashing_start = match player.dashing_start {
                Some(x) => x,
                None => Instant::now(),
            };

            // FIXME: Sprite and Animation should probably be optional here.
            let sprite = match sprite_components.get_mut(*e) {
                Some(x) => x,
                None => continue,
            };

            let animation = match animation_components.get_mut(*e) {
                Some(x) => x,
                None => continue,
            };

            if player_gamepad.jump {
                if !player.jumping {
                    physics.velocity.y = -player.jump_speed;
                    player.jumping_start = Some(Instant::now());
                    player.jumping = true;
                } else if jumping_start.elapsed().as_millis() < 200 {
                    physics.velocity.y = -player.jump_speed;
                }
            } else if jumping_start.elapsed().as_millis() > 1 {
                player.jumping = false;
                player.jumping_start = None;
            }

            if player_gamepad.use_powerup && !player.dashing {
                if let Some(powers) = powerups_components.get_mut(*e) {
                    // This is to avoid dequeue powerups that are not going to
                    // be handled by 'power' button.
                    if !powers.next_in(&[Powerup::DoubleJump]) {
                        let first_power_wrapper = powers.pop();
                        if let Some(first_power) = first_power_wrapper {
                            // FIXME: All powers are NOT going to be handled
                            //        here, for instance, DoubleJump should be
                            //        handled in the jump key.
                            match first_power {
                                // FIXME: This is only for development
                                //        proppses, here we should implement
                                //        the powerups that are going to be
                                //        fiered by the 'power' key.
                                Powerup::Dash => dash(player, physics, sprite.flip_x),
                                Powerup::DoubleJump => println!("DoubleJump"),
                            }
                        }
                    }
                }
            }

            if player.dashing {
                if dashing_start.elapsed().as_millis() >= player.dash_duration as u128 {
                    player.dashing = false;
                    player.dashing_start = None;
                    physics.velocity.x = 0.0;
                    physics.acceleration.y = 900.0;
                } else {
                    continue; // Ignore input left/right during dash.
                }
            }

            // FIXME: The player's collider needs to be modified according
            //        to the player's looking direction (if the collider is
            //        off-center)

            if player_gamepad.left {
                physics.velocity.x = -player.speed;

                sprite.flip_x = true;
                animation.current_clip = 1;
            } else if player_gamepad.right {
                physics.velocity.x = player.speed;

                sprite.flip_x = false;
                animation.current_clip = 1;
            } else {
                physics.velocity.x = 0.0;

                animation.current_clip = 0;
            };
        }
    }
}

// Powerups:
fn dash(mut player: &mut Player, mut physics: &mut Physics, left: bool) {
    player.dashing = true;
    player.dashing_start = Some(Instant::now());
    physics.acceleration.y = 0.0;

    if left {
        physics.velocity = Vec2::new(-player.dash_speed, 0.0);
    } else {
        physics.velocity = Vec2::new(player.dash_speed, 0.0);
    }
}
