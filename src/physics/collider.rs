use pimba::rect::Rect;

use super::tag::Tag;

#[derive(Debug)]
pub struct Collider {
    pub shape: Rect,
    pub sensor: bool,
    pub tag: Tag,
}
