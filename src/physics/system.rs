use std::cmp::{max, min};
use std::vec::Vec;

use pimba::rect::Rect;

use super::collider::Collider;
use super::collider_map::{ColliderMap, ColliderTile};
use super::contact::Contact;
use super::tag::Tag;

use crate::components::{ColliderComponents, PhysicsComponents, TransformComponents};

#[derive(Clone, Debug)]
struct Quad {
    x0: f32,
    y0: f32,
    x1: f32,
    y1: f32,
}

pub fn physics_system(
    dt: f32, // FIXME: Use a proper `time` type for this.
    collider_map: &ColliderMap,
    contact_events: &mut Vec<Contact>,
    transform_components: &mut TransformComponents,
    physics_components: &mut PhysicsComponents,
    collider_components: &ColliderComponents,
) {
    contact_events.clear();

    for (physics, e) in physics_components.iter_mut() {
        let transform = match transform_components.get_mut(*e) {
            Some(x) => x,
            None => continue,
        };

        let previous_position = transform.position.clone();

        physics.velocity = physics.velocity + physics.acceleration * dt;
        transform.position = transform.position + physics.velocity * dt;

        if let Some(collider) = collider_components.get(*e) {
            let shape = &collider.shape;

            let a = Quad {
                x0: shape.x() as f32 + previous_position.x,
                y0: shape.y() as f32 + previous_position.y,
                x1: (shape.x() + shape.width()) as f32 + previous_position.x,
                y1: (shape.y() + shape.height()) as f32 + previous_position.y,
            };

            let b = Quad {
                x0: shape.x() as f32 + transform.position.x,
                y0: shape.y() as f32 + transform.position.y,
                x1: (shape.x() + shape.width()) as f32 + transform.position.x,
                y1: (shape.y() + shape.height()) as f32 + transform.position.y,
            };

            let q = resolve_collisions_with_map(&a, &b, collider, collider_map, contact_events);

            if !collider.sensor {
                transform.position.x = q.x0 - shape.x() as f32;
                transform.position.y = q.y0 - shape.y() as f32;

                if q.x0 != b.x0 {
                    physics.velocity.x = 0.0;
                }

                if q.y0 != b.y0 {
                    physics.velocity.y = 0.0;
                }
            }
        }
    }
}

fn resolve_quad_against_tile_collisions(
    a: &Quad,
    b: &Quad,
    j: i32,
    i: i32,
    shape: &Rect,
    map: &ColliderMap,
) -> Option<Quad> {
    let tile = Quad {
        x0: (j * map.tile_width()) as f32,
        y0: (i * map.tile_height()) as f32,
        x1: ((j + 1) * map.tile_width()) as f32,
        y1: ((i + 1) * map.tile_height()) as f32,
    };

    if a.x1 <= tile.x0
        && b.x1 >= tile.x0
        && a.y0.min(b.y0) <= tile.y1
        && a.y1.max(b.y1) >= tile.y0
        && map.at(j - 1, i) == ColliderTile::Empty
    {
        Some(Quad {
            x0: tile.x0 - shape.width() as f32,
            x1: tile.x0,
            y0: b.y0,
            y1: b.y1,
        })
    } else if a.x0 >= tile.x1
        && b.x0 <= tile.x1
        && a.y0.min(b.y0) <= tile.y1
        && a.y1.max(b.y1) >= tile.y0
        && map.at(j + 1, i) == ColliderTile::Empty
    {
        Some(Quad {
            x0: tile.x1,
            x1: tile.x1 + shape.width() as f32,
            y0: b.y0,
            y1: b.y1,
        })
    } else if a.y1 <= tile.y0
        && b.y1 >= tile.y0
        && a.x0.min(b.x0) <= tile.x1
        && a.x1.max(b.x1) >= tile.x0
        && map.at(j, i - 1) == ColliderTile::Empty
    {
        Some(Quad {
            x0: b.x0,
            x1: b.x1,
            y0: tile.y0 - shape.height() as f32,
            y1: tile.y0,
        })
    } else if a.y0 >= tile.y1
        && b.y0 <= tile.y1
        && a.x0.min(b.x0) <= tile.x1
        && a.x1.max(b.x1) >= tile.x0
        && map.at(j, i + 1) == ColliderTile::Empty
    {
        Some(Quad {
            x0: b.x0,
            x1: b.x1,
            y0: tile.y1,
            y1: tile.y1 + shape.height() as f32,
        })
    } else {
        None
    }
}

fn resolve_collisions_with_map(
    a: &Quad,
    b: &Quad,
    collider: &Collider,
    map: &ColliderMap,
    contact_events: &mut Vec<Contact>,
) -> Quad {
    // Compute the area of potentially colliding map tiles based
    // on the starting and ending positions (`a` -> `b`)
    //
    //     map_x0 ... map_x1
    //        |          |
    //     map_y0 ... map_y1
    //
    // FIXME: The are of potentially-colliding-tiles is increased by 1
    //        in each direction to prevent missed collisions in some
    //        cases (possibly due to Quad using f32 instead of i32
    //               as it did previously).
    let map_x0 = (min(a.x0 as i32, b.x0 as i32) - 1) / map.tile_width();
    let map_y0 = (min(a.y0 as i32, b.y0 as i32) - 1) / map.tile_height();

    let map_x1 = (max(a.x1 as i32, b.x1 as i32) + 1) / map.tile_width();
    let map_y1 = (max(a.y1 as i32, b.y1 as i32) + 1) / map.tile_height();

    let mut out = b.clone();
    // FIXME: This needs to conditionally iterate in the direction
    //        of movement (e.g: map_x0 -> map_x1 if a.x0 <= b.x0 or
    //                          map_x1 -> map_x0 if a.x0 > b.x0)
    //        (The same for map_y0, map_y1)
    for i in map_y0..=map_y1 {
        for j in map_x0..=map_x1 {
            match map.at(j, i) {
                ColliderTile::Rect => {
                    if let Some(q) =
                        resolve_quad_against_tile_collisions(&a, &out, j, i, &collider.shape, &map)
                    {
                        out = q;
                        contact_events.push(Contact {
                            first: collider.tag,
                            second: Tag::Wall, // FIXME: Properly compute the tile's tag
                        });
                    }
                }
                _ => {}
            }
        }
    }
    out
}
