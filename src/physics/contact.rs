use super::tag::Tag;

#[derive(Debug)]
pub struct Contact {
    pub first: Tag,
    pub second: Tag,
    // FIXME: Add contact points
}
