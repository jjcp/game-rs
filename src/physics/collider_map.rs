use std::vec::Vec;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
#[repr(u8)]
pub enum ColliderTile {
    Empty,
    Rect,
}

pub struct ColliderMap {
    tiles: Vec<ColliderTile>,
    width: i32,
    height: i32,
    tile_width: i32,
    tile_height: i32,
}

impl ColliderMap {
    pub fn new(
        tiles: Vec<ColliderTile>,
        width: i32,
        height: i32,
        tile_width: i32,
        tile_height: i32,
    ) -> ColliderMap {
        assert!(width * height == tiles.len() as i32);

        assert!(width >= 0);
        assert!(height >= 0);
        assert!(tile_width >= 0);
        assert!(tile_height >= 0);

        ColliderMap {
            tiles: tiles,
            width: width,
            height: height,
            tile_width: tile_width,
            tile_height: tile_height,
        }
    }

    pub fn width(&self) -> i32 {
        self.width
    }

    pub fn height(&self) -> i32 {
        self.height
    }

    pub fn tile_width(&self) -> i32 {
        self.tile_width
    }

    pub fn tile_height(&self) -> i32 {
        self.tile_height
    }

    pub fn tiles(&self) -> &[ColliderTile] {
        self.tiles.as_slice()
    }

    pub fn tiles_mut(&mut self) -> &mut [ColliderTile] {
        self.tiles.as_mut_slice()
    }

    pub fn at(&self, x: i32, y: i32) -> ColliderTile {
        let index = y * self.width + x;

        if index >= 0 && index < self.tiles.len() as i32 {
            self.tiles[index as usize]
        } else {
            ColliderTile::Empty
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_construction_of_a_collider_map() {
        use ColliderTile::{Empty, Rect};

        let ctm = ColliderMap::new(
            vec![Empty, Rect, Empty, Rect, Empty, Rect, Empty, Rect],
            4,
            2,
            16,
            32,
        );

        assert_eq!(ctm.width(), 4);
        assert_eq!(ctm.height(), 2);
        assert_eq!(ctm.tile_width(), 16);
        assert_eq!(ctm.tile_height(), 32);

        assert_eq!(ctm.at(0, 0), Empty);
        assert_eq!(ctm.at(1, 0), Rect);
        assert_eq!(ctm.at(2, 0), Empty);
        assert_eq!(ctm.at(3, 0), Rect);

        assert_eq!(ctm.at(0, 1), Empty);
        assert_eq!(ctm.at(1, 1), Rect);
        assert_eq!(ctm.at(2, 1), Empty);
        assert_eq!(ctm.at(3, 1), Rect);

        assert_eq!(ctm.at(-1, 0), Empty);
        assert_eq!(ctm.at(0, -1), Empty);
        assert_eq!(ctm.at(4 * 2, 0), Empty);
        assert_eq!(ctm.at(0, 3), Empty);
        assert_eq!(ctm.at(10, 10), Empty);
    }
}
