pub mod collider;
pub mod collider_map;
pub mod contact;
pub mod system;
pub mod tag;

use pimba::math::Vec2;

#[derive(Debug)]
pub struct Physics {
    pub velocity: Vec2,
    pub acceleration: Vec2,
}
