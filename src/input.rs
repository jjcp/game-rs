use sdl2::keyboard::Keycode;
use std::vec::Vec;

#[derive(Debug, Copy, Clone)]
pub struct PlayerGamepad {
    pub jump: bool,
    pub left: bool,
    pub right: bool,
    pub use_powerup: bool,
}

pub struct InputMapper {
    players_gamepad: Option<Vec<PlayerGamepad>>,
}

impl InputMapper {
    pub fn new() -> InputMapper {
        InputMapper {
            players_gamepad: Some(Vec::new()),
        }
    }

    pub fn add_player_gamepad(&mut self) {
        let players_gamepad = match &mut self.players_gamepad {
            Some(x) => x,
            None => panic!("InputMapper should have a players_gamepad created!"),
        };

        players_gamepad.push(PlayerGamepad {
            jump: false,
            left: false,
            right: false,
            use_powerup: false,
        })
    }

    pub fn key_pressed(&mut self, keycode: Keycode) {
        self.change_key_on_gamepad(keycode, true);
    }

    pub fn key_released(&mut self, keycode: Keycode) {
        self.change_key_on_gamepad(keycode, false);
    }

    pub fn get_player_gamepad(&self, pos: u8) -> PlayerGamepad {
        let players_gamepad = match &self.players_gamepad {
            Some(x) => x,
            None => panic!("Create a player before calling get_player_gamepad."),
        };
        return players_gamepad[pos as usize];
    }

    fn change_key_on_gamepad(&mut self, keycode: Keycode, value: bool) {
        let fill_players_gamepads = match &mut self.players_gamepad {
            Some(x) => x.len() > 0,
            None => false,
        };

        // FIXME: this is no taking in account multiple possible players
        // FIXME: We should have somewhere the keys maps that we will use to
        // fill the PlayerGamepads.
        if fill_players_gamepads {
            let players_gamepad = match &mut self.players_gamepad {
                Some(x) => x,
                None => panic!("The gamepad should have a players_gamepad!"),
            };
            match keycode {
                Keycode::J => {
                    players_gamepad[0].jump = value;
                }
                Keycode::A => {
                    players_gamepad[0].left = value;
                }
                Keycode::D => {
                    players_gamepad[0].right = value;
                }
                Keycode::K => {
                    players_gamepad[0].use_powerup = value;
                }
                _ => {}
            }
        }
    }
}
