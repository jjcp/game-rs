use pimba::ecs::entity::Entity;

use super::Camera;

use crate::components::TransformComponents;

pub fn camera_system(
    dt: f32, // FIXME: Use a proper deltatime type.
    entity_to_follow: Entity,
    camera: &mut Camera,
    transform_components: &TransformComponents,
) {
    if !entity_to_follow.is_invalid() {
        if let Some(transform) = transform_components.get(entity_to_follow) {
            let speed: f32 = 2.0; // FIXME: << This.
            camera.position.x += dt * speed * (transform.position.x - camera.position.x);
            camera.position.y += dt * speed * (transform.position.y - camera.position.y);
        }
    }
}
