pub mod system;

use pimba::math::Vec2;

pub struct Camera {
    pub position: Vec2,
    pub scale: Vec2,
}
