use std::vec::Vec;

use pimba::ecs::world::World;

use crate::components::{
    AnimationComponents, ColliderComponents, PhysicsComponents, PlayerComponents,
    PowerupsComponents, SpriteComponents, TransformComponents,
};
use crate::input::InputMapper;
use crate::physics::contact::Contact;

pub struct GameState {
    pub world: World,
    pub input_mapper: InputMapper,
    pub contact_events: Vec<Contact>,
    pub transform_components: TransformComponents,
    pub sprite_components: SpriteComponents,
    pub animation_components: AnimationComponents,
    pub physics_components: PhysicsComponents,
    pub collider_components: ColliderComponents,
    pub player_components: PlayerComponents,
    pub powerups_components: PowerupsComponents,
}

impl GameState {
    pub fn new() -> GameState {
        GameState {
            world: World::new(),
            input_mapper: InputMapper::new(),
            contact_events: Vec::new(),
            transform_components: TransformComponents::new(),
            sprite_components: SpriteComponents::new(),
            animation_components: AnimationComponents::new(),
            physics_components: PhysicsComponents::new(),
            collider_components: ColliderComponents::new(),
            player_components: PlayerComponents::new(),
            powerups_components: PowerupsComponents::new(),
        }
    }
}
