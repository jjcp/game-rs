use std::vec::Vec;

use super::clip::Clip;

#[derive(Debug)]
pub struct Animation {
    pub clips: Vec<Clip>,
    pub current_clip: usize,
    pub play: bool,
    // FIXME: Use a proper `time` type here
    pub seconds_per_frame: f32,
    pub current_counter: f32,
}
