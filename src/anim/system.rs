use crate::components::{AnimationComponents, SpriteComponents};

pub fn animation_system(
    dt: f32, // FIXME: Use a proper `time` type here.
    sprite_components: &mut SpriteComponents,
    animation_components: &mut AnimationComponents,
) {
    for (animation, e) in animation_components.iter_mut() {
        let sprite = match sprite_components.get_mut(*e) {
            Some(x) => x,
            None => continue,
        };

        if animation.play {
            animation.current_counter += dt;

            if animation.current_counter >= animation.seconds_per_frame {
                animation.current_counter = 0.0;

                let mut clip = &mut animation.clips[animation.current_clip];
                clip.index = (clip.index + 1) % clip.frames.len();

                sprite.rect = clip.frames[clip.index].clone();
            }
        }
    }
}
