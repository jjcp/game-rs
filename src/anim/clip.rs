use std::vec::Vec;

use pimba::rect::Rect;

#[derive(Debug)]
pub struct Clip {
    pub frames: Vec<Rect>,
    pub index: usize,
}
